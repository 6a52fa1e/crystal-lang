# Greet ${USER} (eg. reading env vars)
user = ENV["USER"] ||= "Anonymous User"
puts "Hi #{user}!"

# ECR templating
require "ecr"
class Template
  def initialize(@user : String,@time : Time,@scheme : String,@host : String,@path : String,@url : String,@heroku : String,@headers : String,@ip : String,@geo : String)
  end
  ECR.def_to_s "template.ecr"
end

# A very basic HTTP server ...
require "http/server"
require "http/client"
bind = ENV["BIND"] ||= "0.0.0.0"
port = ENV["PORT"] ||= "8080"
port = port.to_i
url = "https://gitlab.com/6a52fa1e/crystal-lang"
heroku = "https://my-crystal-lang-app.herokuapp.com"

server = HTTP::Server.new do |context|
  headers = context.request.headers.to_s
  host = context.request.headers["Host"] ||= "127.0.0.1"
  ip = context.request.headers["X-Forwarded-For"] ||= "127.0.0.1"
  scheme = context.request.headers["X-Forwarded-Proto"] ||= "http"
  path = context.request.path
  response = HTTP::Client.get "https://ifconfig.co/json?ip=#{ip}"
  geo = response.body
  context.response.content_type = "text/html"
  context.response.print Template.new(user,Time.local,scheme,host,path,url,heroku,headers,ip,geo).to_s
end
address = server.bind_tcp bind, port
puts "Listening on http://#{address}"
server.listen
