# crystal-lang sandbox

#### description

`docker-compose` to easily try out `crystal-lang`

#### usage

    # docker-compose up --build --remove-orphans

#### sample output

    # docker-compose up --build --remove-orphans
    [+] Running 1/1
     ⠿ Container crystal  Recreated                                                                                              0.1s
    Attaching to crystal
    crystal  | Hi Anonymous User!
    crystal  | Listening on http://0.0.0.0:8080

Now open <http://127.0.0.1:8080> in your browser

Or from another `terminal`, run a `curl` request :

    # curl -Lks 127.0.0.1:8080
    Hello Anonymous User!
    So, you wanted to check out some content at http://0.0.0.0:8080/, is that right?
    Nothing to see there... yet
    Now move along!
    Thanks for trying out Crystal though <3
    For more info head to https://crystal-lang.org/
    Bye!

#### crystal runs from heroku!

Open <https://my-crystal-lang-app.herokuapp.com> in your browser

#### more info

- <https://crystal-lang.org>
